﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameflow : MonoBehaviour
{
    public Transform tile1Obj;
    private Vector3 nextTileSpawn;
    public Transform brickObj;
    private Vector3 nextBrickSpawn;
    private int randX;
    public Transform smCrateObj;
    private Vector3 nextSmCrateSpawn;
    public Transform dbCrateObj;
    private Vector3 nextDbCrateSpawn;
    public Transform cartObj;
    private Vector3 nextCartSpawn;
    private int randChoice;
    public static int totalCoins = 0;
    public Transform coinObj;
    private Vector3 nextRampSpawn;
    public Transform rampObj;

    void Start()
    {
        nextTileSpawn.z = 21;
        StartCoroutine(spawnTile());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //public GameObject obstaclePrefab;

    IEnumerator spawnTile()
    {
        yield return new WaitForSeconds(1);
        randX = Random.Range(-1, 2);
        nextBrickSpawn = nextTileSpawn;
        nextBrickSpawn.y = -1.22f;
        nextBrickSpawn.x = randX; 
        Instantiate(tile1Obj, nextTileSpawn, tile1Obj.rotation);
        Instantiate(brickObj, nextBrickSpawn, brickObj.rotation);


        nextTileSpawn.z += 7;
        randX = Random.Range(0, 2);
        nextSmCrateSpawn.z = nextTileSpawn.z;
        nextSmCrateSpawn.y = -0.812f;
        nextSmCrateSpawn.x = randX;
        Instantiate(tile1Obj, nextTileSpawn, tile1Obj.rotation);
        Instantiate(smCrateObj, nextSmCrateSpawn, smCrateObj.rotation);

        if (randX == 0){
            randX = 1;
        }
        else 
        if (randX == 1){
            randX = -1;
        }
        else{
            randX = 0;
        }

        randChoice = Random.Range(0, 3);
        if (randChoice == 0)
        {
            nextDbCrateSpawn.z = nextTileSpawn.z;
            nextDbCrateSpawn.y = -1.282f;
            nextDbCrateSpawn.x = randX;
            Instantiate(dbCrateObj, nextDbCrateSpawn, dbCrateObj.rotation);
        }
        else if (randChoice == 1)
        {
            nextCartSpawn.z = nextTileSpawn.z;
            nextCartSpawn.y = -1.13f;
            nextCartSpawn.x = randX;
            Instantiate(cartObj, nextCartSpawn, cartObj.rotation);
        }
        else// if (randChoice == 2)
        {
            nextCartSpawn.z = nextTileSpawn.z;
            nextCartSpawn.y = -0.675f;
            nextCartSpawn.x = randX;
            Instantiate(coinObj, nextCartSpawn, coinObj.rotation);
        }
         /*else
        {
            nextRampSpawn.z = nextTileSpawn.z;
            nextRampSpawn.y = -0.675f;
            nextRampSpawn.x = randX;
            Instantiate(rampObj, nextRampSpawn, rampObj.rotation);
        }*/

        nextTileSpawn.z += 7;
        StartCoroutine(spawnTile());
    }
}
