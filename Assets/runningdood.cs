﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class runningdood : MonoBehaviour
{
    private string laneChange = "n";
    private string midJump = "n";
    public static Vector3 doodPos;

    bool alive = true;
   
    void Start()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 3);
    }

    
    void Update()
    {
        if (!alive) return; 

        doodPos = transform.position;

        if (Input.GetKey("a") && (laneChange == "n") && (transform.position.x > -.9) && (midJump == "n"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(-1, 0, 3);
            laneChange = "y";
            StartCoroutine(stopLaneCh());
        }
        if (Input.GetKey("d") && (laneChange == "n") && (transform.position.x < .9) && (midJump == "n"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(1, 0, 3);
            laneChange = "y";
            StartCoroutine(stopLaneCh());
        }

        if (Input.GetKey("space") && (midJump == "n") && (laneChange == "n"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 2, 3);
            midJump = "y";
            StartCoroutine(stopJump());
        }
    }

    IEnumerator stopJump()
    {
        yield return new WaitForSeconds(.75f);
        GetComponent<Rigidbody>().velocity = new Vector3(0, -2, 3);
        yield return new WaitForSeconds(.75f);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 3);
        midJump = "n";
    }

    IEnumerator stopLaneCh()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 3);
        laneChange = "n";
        //Debug.Log(GetComponent<Transform>().position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "obstacle")
        {
            Debug.Log("ouch!");
        }

        if (other.tag == "ramp")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 3, 3);
        }

        if (other.tag == "dropdown")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, -3, 3);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ramp")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 3);
        }

        if (other.tag == "dropdown")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 3);
        }
    }

    public void die()
    {
        alive = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}
