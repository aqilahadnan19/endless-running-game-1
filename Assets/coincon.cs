﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coincon : MonoBehaviour
{
    
    void Start()
    {
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 5, 0);
    }

    
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "dood")
        {
            gameflow.totalCoins += 1;
            Debug.Log(gameflow.totalCoins);
            Destroy(gameObject);
        }

        GameManager.inst.IncrementScore();
        
    }
}
